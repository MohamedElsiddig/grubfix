# grubfix
Fix your grub after installing windows
# Usage:

Run the script from linux live cd (Tested on ubuntu):

* To manually enter the linux installation partition run

```bash
sudo ./grub_fix.sh
```

* To automatically detect the linux partition run

```bash
sudo ./grub_fix_autoDetect.sh
```
